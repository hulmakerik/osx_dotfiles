# osx_dotfiles

* Odstranit dolní listu
    * https://apple.stackexchange.com/questions/59556/is-there-a-way-to-completely-disable-dock
* 10 virtualnich ploch, ovládané pomoci win+number
    * https://github.com/Jaysce/Spaceman
* Odstranit animace
    * Accessibility - reduce motion
* Schovat nepotřebné aplikace
* Umistovani oken
* Zajistit aby se funkcni tlačítka chovaly jako kontrolu a aby to nebylo cele divne nelogicky rozmístěné na ostatní tlacitka
* Zshrc
* Touchpad - nastavit relevantní gesta
* Ulozit oznaceny text do clipboard automaticky
* Nauc se jak skriptovat os
* Kdyz zmačkánu krizek, tak chci aby se program zavřel
    * Srát na tlačítka, používej klávesnici
* Myš zrychluje nelineárne, scroll taky
    * defaults write .GlobalPreferences com.apple.mouse.scaling -1
* Alt tab nefunguje - musíš si stáhnout plugin co není na App Store :D
    * Alt tab je naprosto nefunkční paranormální rondom funkce
    * https://alt-tab-macos.netlify.app/
* Na clipování oken si musíš koupit magnetic rozšíření :D
    * https://github.com/ianyh/Amethyst
    * https://www.spectacleapp.com/
* Klávesová zkratka na spuštění aplikace - launch document v authomateru, potom v nastavení přiřadit. Ve zkratce nesmí být enter :D
* Ikony ve složce jsou mimo grid
