# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.config/oh-my-zsh"
ZSH_THEME="robbyrussell"




# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
ZSH_CUSTOM="$HOME/.config/oh-my-zsh"

plugins=(git zsh-autosuggestions)

source $ZSH/oh-my-zsh.sh

# https://stackoverflow.com/questions/72046196/i-have-a-custom-alias-for-ls-als-called-la-in-my-zshrc-and-its-being-over
export ENV_DIR="$HOME/.config/erik_env"
[ -f "$ENV_DIR/varrc" ] && source "$ENV_DIR/varrc"
[ -f "$ENV_DIR/locationrc" ] && source "$ENV_DIR/locationrc"
[ -f "$ENV_DIR/aliasrc" ] && source "$ENV_DIR/aliasrc"
