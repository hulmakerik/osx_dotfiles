let mapleader=";"
set title 			" if the terminal has tab, show title
set bg=light
set go=a
set mouse=a
set autoindent 			" copy an indentation from previous line
set smartindent 		" inserts one more indentation in some cases
set softtabstop=4 		" number of spaces in TAB when editing
set tabstop=4 			" number of visual spaces per TAB
set shiftwidth=4 		" affects what happens when you press >> << or ==
set expandtab 			" TABs are spaces
set noerrorbells 		" disable error bells
set nohlsearch
set noshowmode
set noruler 			" do not show cursor position in bottom bar
set laststatus=0
set scrolloff=10 		" start scrolling x lines earlier
set noshowcmd
set cursorline 			" highlight current line
set nocompatible
syntax on 			" enable syntax highlighting
filetype plugin on 		" syntax highlighting based on filetype
set encoding=utf8 		" set encoding to utf-8
set number relativenumber
set wildmode=longest,list,full
set clipboard=unnamedplus 	" vim clipbord is the same one as the system clipboard
au  FocusLost * :wa 		" save the file on focus out

" display visual colored line and set it's color
" set colorcolumn=90
" highlight ColorColumn ctermbg=grey

" Remap Ctrl+j to find <++> tag, delete it and start editing
"     in both insert and normal mode without saving it to buffer
inoremap <c-j> <Esc>/<++><Enter>"_c4l
nnoremap <c-j> /<++><Enter>"_c4l

nnoremap <c-s> :w<Enter> 	" remap Ctrl+s to save file in normal mode

" Automatically deletes all trailing whitespace and newlines at end of file on save.
autocmd BufWritePre * %s/\s\+$//e
autocmd BufWritePre * %s/\n\+\%$//e
autocmd BufWritePre *.[ch] %s/\%$/\r/e

" remap ';i' to wtire '#include <>' or 'import'
autocmd FileType cpp nnoremap <leader>i ggO#include <><Esc>i
autocmd FileType py nnoremap <leader>i ggOimport<Esc>i


call plug#begin()
Plug 'navarasu/onedark.nvim'
Plug 'tpope/vim-sensible'
Plug 'preservim/nerdtree'
Plug 'bling/vim-airline'
Plug 'frazrepo/vim-rainbow'		" rainbow brackets
Plug 'vim-airline/vim-airline-themes'
Plug 'ryanoasis/vim-devicons'
Plug 'shime/vim-livedown'
Plug 'junegunn/fzf.vim'			" finder - history, files, colors, commands etc.
Plug 'junegunn/fzf'			" finder - history, files, colors, commands etc.
Plug 'tpope/vim-fugitive'  		" vim plugin for vim
Plug 'ghifarit53/tokyonight-vim'
call plug#end()

" airline config
" let g:airline#extensions#tabline#enabled = 1  " show tabs
let g:airline_left_sep = ''
let g:airline_right_sep = ''
let g:airline#extensions#tabline#left_sep = ''
let g:airline#extensions#tabline#left_alt_sep = ''

let g:rainbow_active = 1 		" rainbow brackets

" nerd tree
map <F2> :NERDTreeToggle<CR>
" FZF usage - https://github.com/junegunn/fzf.vim
map <F3> :History<CR>

" colorscheme
set termguicolors
let g:tokyonight_style = 'night' " available: night, storm
let g:tokyonight_enable_italic = 1
let g:airline_theme = "tokyonight"
colorscheme tokyonight

